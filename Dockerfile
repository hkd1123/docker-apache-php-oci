FROM ubuntu:16.04

LABEL maintainer="allconnect."

RUN apt-get update && apt-get install -y --no-install-recommends \
        apache2 \
        software-properties-common \
        supervisor \
    && apt-get clean \
    && rm -fr /var/lib/apt/lists/*

RUN LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php

RUN apt-get update && apt-get install -y --no-install-recommends \
        php7.0-dev \
        libaio-dev \
        build-essential \
    && apt-get clean \
    && rm -fr /var/lib/apt/lists/*


RUN apt-get update && apt-get install -y --no-install-recommends \
        libapache2-mod-php7.0 \
        php7.0 \
        php7.0-cli \
        php7.0-curl \
        php7.0-dev \
        php7.0-gd \
        php7.0-imap \
        php7.0-mbstring \
        php7.0-mcrypt \
        php7.0-mysql \
        php7.0-pgsql \
        php7.0-pspell \
        php7.0-xml \
        php7.0-xmlrpc \
        php7.0-zip \
        php-apcu \
        php-memcached \
        php-pear \
        php-redis \
        vim \
        curl \
        zip \
        unzip \
    && apt-get clean \
    && rm -fr /var/lib/apt/lists/*


RUN a2enmod rewrite ssl

COPY conf/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY conf/default-ssl.conf /etc/apache2/sites-enabled/default-ssl.conf
COPY conf/security.conf /etc/apache2/conf-enabled/security.conf

COPY key/server.crt /etc/ssl/certs/ssl-cert-snakeoil.pem
COPY key/server.key /etc/ssl/private/ssl-cert-snakeoil.key

COPY conf/php70.ini /etc/php/7.0/apache2/php.ini

COPY conf/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

COPY script/run.sh /run.sh
RUN chmod 755 /run.sh

COPY conf/config /config


# Oracle instantclient
ADD instantclient-basic-linux.x64-12.1.0.2.0.zip /tmp/
ADD instantclient-sdk-linux.x64-12.1.0.2.0.zip /tmp/
ADD instantclient-sqlplus-linux.x64-12.1.0.2.0.zip /tmp/

RUN unzip /tmp/instantclient-basic-linux.x64-12.1.0.2.0.zip -d /usr/local/
RUN unzip /tmp/instantclient-sdk-linux.x64-12.1.0.2.0.zip -d /usr/local/
RUN unzip /tmp/instantclient-sqlplus-linux.x64-12.1.0.2.0.zip -d /usr/local/
RUN ln -s /usr/local/instantclient_12_1 /usr/local/instantclient
RUN ln -s /usr/local/instantclient/libclntsh.so.12.1 /usr/local/instantclient/libclntsh.so
RUN ln -s /usr/local/instantclient/sqlplus /usr/bin/sqlplus
RUN echo 'instantclient,/usr/local/instantclient' | pecl install oci8
RUN echo "extension=oci8.so" > /etc/php/7.0/apache2/conf.d/30-oci8.ini
RUN echo "extension=oci8.so" > /etc/php/7.0/cli/conf.d/30-oci8.ini

EXPOSE 80 
EXPOSE 443
CMD ["/run.sh"]
